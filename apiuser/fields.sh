#!/bin/bash

. .local_url

METHOD="fields"

(
  echo '{ "jsonrpc":"2.0", "method":"'${METHOD}'",'
  echo '"params":{'
  met_cli_auth
  echo '},'
  echo '"id":'`date +%s`
  echo '}'
) > tmp.json

met_call_api tmp.json
met_output
