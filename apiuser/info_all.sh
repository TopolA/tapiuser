#!/bin/bash

. .local_url

METHOD="info"

(
  echo '{ "jsonrpc":"2.0", "method":"'${METHOD}'",'
  echo '"params":{'
  met_cli_auth
  echo '  "system":true,'
  echo '  "prov":true,'
  echo '  "methods":true,'
  echo '  "fields":true,'
  echo '  "resources":true,'
  echo '},'
  echo '"id":'`date +%s`
  echo '}'
) > tmp.json

met_call_api tmp.json
met_output
